package pl.javastart.dnd;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class AnimalServiceTest {

    @Mock
    AnimalRepository animalRepository;
    @Captor
    ArgumentCaptor<Animal> animalCaptor;
    @Captor
    ArgumentCaptor<List<Animal>> animalListCaptor;

    private AnimalService animalService;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        animalService = new AnimalService(animalRepository);
    }

    @Test
    void shouldCorrectlyCalculateSortOrder_WhenAnimalMovedToStart() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        List<Animal> animals = createList(horse, goat);

        //when
        animalService.moveAndSaveAnimal(0, animals, goat);

        //then
        Mockito.verify(animalRepository).save(animalCaptor.capture());
        Animal capturedAnimal = animalCaptor.getValue();
        assertThat(capturedAnimal.getSortOrder()).isEqualTo(-100);
    }

    @Test
    void shouldCorrectlyCalculateSortOrder_WhenAnimalMovedToEnd() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        List<Animal> animals = createList(horse, goat);

        //when
        animalService.moveAndSaveAnimal(1, animals, horse);

        //then
        Mockito.verify(animalRepository).save(animalCaptor.capture());
        Animal capturedAnimal = animalCaptor.getValue();
        assertThat(capturedAnimal.getSortOrder()).isEqualTo(200);
    }

    @Test
    void shouldCorrectlyCalculateSortOrder_WhenAnimalMovedToBetweenFromEnd() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        Animal dog = createAnimal("Pies", 200);
        Animal cat = createAnimal("Kot", 300);
        List<Animal> animals = createList(horse, goat, dog, cat);

        //when
        animalService.moveAndSaveAnimal(1, animals, cat);

        //then
        Mockito.verify(animalRepository).save(animalCaptor.capture());
        Animal capturedAnimal = animalCaptor.getValue();
        assertThat(capturedAnimal.getSortOrder()).isEqualTo(50);
    }

    @Test
    void shouldCorrectlyCalculateSortOrder_WhenAnimalMovedToBetweenFromStart() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        Animal dog = createAnimal("Pies", 200);
        Animal cat = createAnimal("Kot", 300);
        List<Animal> animals = createList(horse, goat, dog, cat);

        //when
        animalService.moveAndSaveAnimal(2, animals, horse);

        //then
        Mockito.verify(animalRepository).save(animalCaptor.capture());
        Animal capturedAnimal = animalCaptor.getValue();
        assertThat(capturedAnimal.getSortOrder()).isEqualTo(250);
    }

    @Test
    void shouldThrowException_WhenNotAbleToCalculateSortOrder() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        Animal dog = createAnimal("Pies", 101);
        Animal cat = createAnimal("Kot", 300);
        List<Animal> animals = createList(horse, goat, dog, cat);

        //then
        assertThatExceptionOfType(UnableToCalculateSortOrderException.class)
                .isThrownBy(() -> animalService.calculateSortOrderForPositionFromMiddle(1, horse, animals));
    }

    @Test
    void shouldCorrectlyRecalculateSortOrderValues() {
        //given
        Animal horse = createAnimal("Koń", 0);
        Animal goat = createAnimal("Koza", 100);
        Animal dog = createAnimal("Pies", 101);
        Animal cat = createAnimal("Kot", 300);
        List<Animal> animals = createList(horse, goat, dog, cat);

        //when
        animalService.moveAndSaveAnimal(1, animals, horse);

        //then
        Mockito.verify(animalRepository).saveAll(animalListCaptor.capture());
        List<Animal> capturedList = animalListCaptor.getValue();
        assertThat(capturedList.get(0).getSortOrder()).isEqualTo(100);
        assertThat(capturedList.get(0).getName()).isEqualTo("Koza");
        assertThat(capturedList.get(1).getSortOrder()).isEqualTo(200);
        assertThat(capturedList.get(1).getName()).isEqualTo("Koń");
        assertThat(capturedList.get(2).getSortOrder()).isEqualTo(300);
        assertThat(capturedList.get(2).getName()).isEqualTo("Pies");
        assertThat(capturedList.get(3).getSortOrder()).isEqualTo(400);
        assertThat(capturedList.get(3).getName()).isEqualTo("Kot");
    }


    private List<Animal> createList(Animal... animals) {
        return new LinkedList<>(Arrays.asList(animals));
    }

    private Animal createAnimal(String name, long sortOrder) {
        Animal animal = new Animal();
        animal.setName(name);
        animal.setSortOrder(sortOrder);
        return animal;
    }

}