package pl.javastart.dnd;

import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnimalService {
    private static final long SORT_ORDER_STEP = 100;

    private AnimalRepository animalRepository;

    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    List<Animal> findAllSorted() {
        return animalRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Animal::getSortOrder))
                .collect(Collectors.toList());
    }

    void move(Long animalId, int targetPosition) {
        List<Animal> sortedAnimals = findAllSorted();
        Optional<Animal> animalOpt = animalRepository.findById(animalId);

        animalOpt.ifPresent(animal -> {
            moveAndSaveAnimal(targetPosition, sortedAnimals, animal);
        });
    }

    void moveAndSaveAnimal(int targetPosition, List<Animal> sortedAnimals, Animal animal) {
        try {
            setNewSortOrderAndSave(targetPosition, sortedAnimals, animal);
        } catch (UnableToCalculateSortOrderException e) {
            recalculateSortOrderValues(targetPosition, animal, sortedAnimals);
        }
    }

    private void setNewSortOrderAndSave(int targetPosition, List<Animal> sortedAnimals, Animal animal) {
        long sortOrder = calculateSortOrder(targetPosition, animal, sortedAnimals);
        animal.setSortOrder(sortOrder);
        animalRepository.save(animal);
    }

    private void recalculateSortOrderValues(int targetPosition, Animal animal, List<Animal> sortedAnimals) {
        sortedAnimals.remove(animal);
        sortedAnimals.add(targetPosition, animal);

        for (int i = 0; i < sortedAnimals.size(); i++) {
            sortedAnimals.get(i).setSortOrder((i + 1) * SORT_ORDER_STEP);
        }

        animalRepository.saveAll(sortedAnimals);
    }

    private long calculateSortOrder(int targetPosition, Animal animal, List<Animal> sortedAnimals) {
        long sortOrder;
        int firstPosition = 0;
        int lastPosition = sortedAnimals.size() - 1;
        long firstElementSortOrder = sortedAnimals.get(firstPosition).getSortOrder();
        long lastElementSortOrder = sortedAnimals.get(lastPosition).getSortOrder();

        if (targetPosition == firstPosition) {
            sortOrder = firstElementSortOrder - SORT_ORDER_STEP;
        } else if (targetPosition == lastPosition) {
            sortOrder = lastElementSortOrder + SORT_ORDER_STEP;
        } else {
            sortOrder = calculateSortOrderForPositionFromMiddle(targetPosition, animal, sortedAnimals);
        }

        return sortOrder;
    }

    long calculateSortOrderForPositionFromMiddle(int targetPosition, Animal animal, List<Animal> sortedAnimals) {
        sortedAnimals.remove(animal);
        long result;
        long previousElementSortOrder = sortedAnimals.get(targetPosition - 1).getSortOrder();
        long nextElementSortOrder = sortedAnimals.get(targetPosition).getSortOrder();

        result = (previousElementSortOrder + nextElementSortOrder) / 2;
        if (result == previousElementSortOrder || result == nextElementSortOrder) {
            throw new UnableToCalculateSortOrderException();
        }

        return result;
    }
}
